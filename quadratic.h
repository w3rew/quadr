#ifndef QUADRATIC_H
#define QUADRATIC_H

/*!
 * \file quadratic.h В этом файле описаны функции и константы, используемые в библиотеке, решающей квадратное уравнение
 */

 /*!
  * \mainpage [readme](https://www.youtube.com/watch?v=PkT0PJwy8mI)
  * */

/*!
 * \brief abs(double) < EPS
 *
 * \param[in] d число для проверки
 * \return является ли число нулем
 **/

bool isZero(double d);
extern const double EPS; ///< константа для isZero, определяющее самое маленькое число, еще не являющееся нулем.

/*!
 * Здесь описаны коды возврата для функций
 */

enum SquareResult{ 
    SS_INF_ROOTS  = 3, ///<infinite number of roots
    SS_NULLPTR = 4,    ///<pointer(s) are NULL
    SS_INF_COEF = 5,   ///<infinite or nan coefficient(s) or discriminant
    SS_OUTPUT = 6,     ///<output error 
    SS_EQUAL = 7       ///<equal pointers x1 == x2 
};

/*!
 * \brief Решает линейное уравнение bx + c = 0, ответ записывает в x
 *
 * \param[in] b,c коэффициенты уравнения
 * \param[in] x указатель для записи ответа
 * \return 0 если корней нет, 1 если корень один, иначе другой код возврата.
 *
 * Если x нулевой, возвращает код ошибки, даже если корень не один. Если корень не один, x не изменяется.
 */

int SolveLinear(double b, double c, double* x);

/*!
 * \brief Решает квадратное уравнение ax^2 + bx + c = 0, корни записываются в x1 и x2.
 *
 * \param[in] a,b,c коэффициенты уравнения
 * \param[in] x1,x2 указатели для записи ответа
 * \return количество корней либо код возврата
 *
 *  Если хотя бы один из указателей нулевой, даже если корень только один, возвращает код SS_NULLPTR. Если корень только один, 
 *  он записывается в x1 и x2 не изменяется. Аналогично если количество корней не равно 2 или 1 x1 и 2 не изменяются.
 */

int SolveSquare(double a, double b, double c, double* x1, double* x2);

#endif
