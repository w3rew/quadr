#include "quadratic.h"
#include <stdio.h>
#include <math.h>

/*!
 * \file tests.c В этом файле определены функции и структура для тестирования SolveSquare
 */

static const double JUNK = 0; ///< константа для индикации того, что некоторый входной параметр не имеет смысла
static double x1, x2;

/*!
 * \brief Структура для хранения входа и ожидаемого выхода для теста SolveSquare
 */
struct Test{
    ///\{
    /*!
     * \brief коэффициенты уравнения
     */
    double a, b, c; 
    ///\}

    ///\{
    /*!
    \brief  указатели, куда нужно будет положить возвращаемые корни.  Добавлено для тестирования неправильных указателей.
    */

    double *x1, *x2; 
    ///\}
    int outCode; ///<ожидаемый код возврата
    ///@{
    /*! \brief ожидаемые значения x1 и x2. Если они не должны быть изменены, рекомендуется установить в JUNK.
     * */
    double ansX1, ansX2; 
};


Test tests[] = {        
    {1,   0, -1,  &x1,  &x2,            2,   -1, 1}, 
    {1,   2,  1,  &x1,  &x2,            1,   -1, JUNK},
    {0,   1,  1,  &x1,  &x2,            1,   -1, JUNK},
    {0,   0,  0,  &x1,  &x2, SS_INF_ROOTS, JUNK, JUNK},
    {0,   0,  1,  &x1,  &x2,            0, JUNK, JUNK},
    {1,   0,  1,  &x1,  &x2,            0, JUNK, JUNK},
    {NAN, 0,  1,  &x1,  &x2,  SS_INF_COEF, JUNK, JUNK},
    {1,   0,  0, NULL, NULL,   SS_NULLPTR, JUNK, JUNK},
    {1,   0,  0,  &x1, NULL,   SS_NULLPTR, JUNK, JUNK},
    {1,   0,  0, NULL,  &x2,   SS_NULLPTR, JUNK, JUNK},
    {1,   0,  0,  &x2,  &x2,     SS_EQUAL, JUNK, JUNK}
};

/*!
 * \brief Производит тестирование SolveSquare исходя из данных test
 * \param[in] test данные для тестирования
 * \return 0 если тест пройден без ошибок, 1 иначе.
 */

int applyTest(Test test) { 
    int result = SolveSquare(test.a, test.b, test.c, test.x1, test.x2);
    if ((test.outCode != result)){
        printf("Test FAILED!\n INPUT: a = %lg, b = %lg, c = %lg, EXPECTED CODE = %d, RECEIVED CODE = %d \n", 
                 test.a, test.b, test.c, test.outCode, result);
            return 1;
    }

    if (result == 1) {
        if (test.x1 != NULL && isZero(x1 - test.ansX1) ) {
            printf("TEST OK!\n");
            return 0;
        }
    } else if (result == 2) {
        if (test.x1 != NULL && test.x2 != NULL && isZero(x1 - test.ansX1)  && isZero(x2 - test.ansX2) ) {
            printf("TEST OK!\n");
            return 0;
        }
    } else {
        printf("TEST OK!\n");
        return 0; 
    }

    printf("Test FAILED!\n INPUT: a = %lg, b = %lg, c = %lg,\
            CORRECT CODE, EXPECTED ANSWER is %lg, %lg RECEIVED ANSWER is %lg, %lg \n", 
            test.a, test.b, test.c, test.ansX1, test.ansX2, x1, x2);
    return 1; //result == 1 || result == 2 but wrong answer
}


int main() {
    for (size_t i = 0; i < sizeof(tests) / sizeof(Test); ++i) {
        int testResult = applyTest(tests[i]);
        if (testResult) return testResult;
    }
    return 0;
}


