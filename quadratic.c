#include "quadratic.h"
#include <stdlib.h>

#ifndef NDEBUG  //disable asserts
#define NDEBUG
#endif

#include <assert.h>
#include <math.h>

/*!
 * \file quadratic.c В этом файле хранится реализация для quadratic.h
 */

const double EPS = 1e-5;

bool isZero(double d) {
    return (abs(d) < EPS);
}

int SolveLinear (double b, double c, double* x) {
    assert (isfinite(b));
    assert (isfinite(c));
    assert(!isnull(x));
    if (!isfinite(b) || !isfinite(c))  return SS_INF_COEF;
    if (!x) return SS_NULLPTR;

    if (isZero(b)) { //b = 0
        if (isZero(c)) return SS_INF_ROOTS; //also c = 0
        else return 0;
    } else {
        *x = -c / b;
        return 1;
    }
}

int SolveSquare(double a, double b, double c, double* x1, double* x2) {
    assert (isfinite(a));
    assert (isfinite(b));
    assert (isfinite(c));
    assert(x1);
    assert(x2);

    if (!isfinite(a) || !isfinite(b) || !isfinite(c)) return SS_INF_COEF; 
    if (!x1 || !x2) return SS_NULLPTR;
    if (x1 == x2) return SS_EQUAL;

    if (isZero(a)) { //linear
        int LinearResult = SolveLinear(b, c, x1);
        return LinearResult;
    } else {
        double D = b*b - 4*a*c;

        if (!isfinite(D) || isnan(D)) return SS_INF_COEF;

        if (D < 0.0) { //no roots
            return 0;
        } else {
            if (isZero(D)) { //one root
                *x1 = -b / 2 / a;
                return 1;
            } else {
                double sqrtD = sqrt(D);
                *x1 = (-b - sqrtD) / 2 / a;
                *x2 = (-b + sqrtD) / 2 / a;
                return 2;
            }
        }
    }
}
