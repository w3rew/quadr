#include <stdio.h>
#include "quadratic.h"

/*!
 * \file main.c В этом файле лежит простейшая программа для ввода и вывода данных для решения квадратных уравнений
 */

/*!
 \brief Печатает результат решения квадратного уравнения
 \param[in] result выход SolveSquare
 \param[in] x1, x2 корни уравнения, найденные SolveSquare.
 \return 0 если вывод был произведен успешно, иначе SS_OUTPUT

 Если корень один, значение x2 не имеет значения. Аналогично, если корней нет, значение x1 и x2 не имеет значения
*/
static int printAnswer(int result, double x1, double x2) {
    int outputResult = -1;
    switch (result) {
        case 0:
             outputResult = printf("0 roots\n");
            break;
        case 1:
            outputResult = printf("One root: %lg\n", x1);
            break;
        case 2:
            outputResult = printf("Two roots: %lg,  %lg\n", x1, x2);
            break;
        case SS_INF_ROOTS:
            outputResult = printf("Any number\n");
            break;
        case SS_NULLPTR:
            outputResult = printf("Error: null pointer(s)\n");
            break;
        case SS_INF_COEF:
            outputResult = printf("Error: infinite coefficients(s)\n");
            break;
        default:
            outputResult = printf("Unknown error\n");
            break;
    }
    if (outputResult < 0) return SS_OUTPUT;
    else return 0;
}
int main() {
    double a = 0, b = 0, c = 0, x1 = 0, x2 = 0;
    printf("Type a b c: ");
    int scanfRead = scanf("%lg %lg %lg", &a, &b, &c);
    while (scanfRead != 3) {
        while(getchar() != '\n'); //consumes wrong input
        printf("Type three floating point numbers a b c correctly: ");
        scanfRead = scanf(" %lg %lg %lg", &a, &b, &c);
    }

    int squareResult = SolveSquare(a, b, c, &x1, &x2);

    int printResult = printAnswer(squareResult, x1, x2);

    return printResult;
}
