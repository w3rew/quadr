var searchData=
[
  ['solvelinear_14',['SolveLinear',['../quadratic_8c.html#afbeb15c6aba7bd93cfefddb22ffb7ff3',1,'SolveLinear(double b, double c, double *x):&#160;quadratic.c'],['../quadratic_8h.html#afbeb15c6aba7bd93cfefddb22ffb7ff3',1,'SolveLinear(double b, double c, double *x):&#160;quadratic.c']]],
  ['solvesquare_15',['SolveSquare',['../quadratic_8c.html#ab2dd9b24e0e487efdd1cd1f40dd3a023',1,'SolveSquare(double a, double b, double c, double *x1, double *x2):&#160;quadratic.c'],['../quadratic_8h.html#ab2dd9b24e0e487efdd1cd1f40dd3a023',1,'SolveSquare(double a, double b, double c, double *x1, double *x2):&#160;quadratic.c']]],
  ['squareresult_16',['SquareResult',['../quadratic_8h.html#aa7c55c71932581005b726a335eb4bb15',1,'quadratic.h']]],
  ['ss_5fequal_17',['SS_EQUAL',['../quadratic_8h.html#aa7c55c71932581005b726a335eb4bb15a0dc48f50ad4860be252e57c123516959',1,'quadratic.h']]],
  ['ss_5finf_5fcoef_18',['SS_INF_COEF',['../quadratic_8h.html#aa7c55c71932581005b726a335eb4bb15a53fb19d435f81b6b681ec321a21f4592',1,'quadratic.h']]],
  ['ss_5finf_5froots_19',['SS_INF_ROOTS',['../quadratic_8h.html#aa7c55c71932581005b726a335eb4bb15ae4c7f7f9f79caf20d1397f3a5d165602',1,'quadratic.h']]],
  ['ss_5fnullptr_20',['SS_NULLPTR',['../quadratic_8h.html#aa7c55c71932581005b726a335eb4bb15a280cc931d49afc071030bd779f737d0b',1,'quadratic.h']]],
  ['ss_5foutput_21',['SS_OUTPUT',['../quadratic_8h.html#aa7c55c71932581005b726a335eb4bb15a851ae91847ff5a84757506bb4b30050c',1,'quadratic.h']]]
];
